const range = (start, end) => new Array(end - start).fill(start).map((el, i) => start + i);

const shortMonth = [
  'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];
const monthVisitData = shortMonth.map(m => {
  return {
    'month': m,
    '投稿数': Math.floor(Math.random() * 1000) + 200,
    'エンゲージメント率': Math.floor(Math.random() * 1000) + 250,
  };
});

const postsSocialData = [
  {
    value: 50,
    name: 'Instagram'
  },
  {
    value: 35,
    name: 'Teitter'
  },
  {
    value: 25,
    name: 'LINE BLOG'
  },
  {
    value: 10,
    name: 'TIK TOK'
  },
  {
    value: 10,
    name: 'Facebook'
  }
];

const getFolowerData = shortMonth.map(m => {
  return {
    'month': m,
    'フォロワー数': Math.floor(Math.random() * 1000) + 200,
    'エンゲージメント率': Math.floor(Math.random() * 1000) + 250,
  };
});
export {
  monthVisitData,
  postsSocialData,
  getFolowerData,
};
