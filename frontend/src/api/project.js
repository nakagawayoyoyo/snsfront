const Projects =  [
  {
    username: 'Dessie',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/ludwiczakpawel/128.jpg',
    name: 'Template PSD',
    deadline: '123233',
    engagement: 90,
    avgiine:4302,
    color: 'pink',
  },
  {
    username: 'Jakayla',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/suprb/128.jpg',    
    name: 'Logo Design',
    deadline: '12201',
    engagement: 70,
    avgiine:310,
    color: 'success'
  },
  {
    username: 'Ludwiczakpawel',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/ludwiczakpawel/128.jpg',    
    name: 'REST API',
    deadline: '33001',
    engagement: 50,
    avgiine:32,
    color: 'info'
  },
  {
    username: 'Damenleeturks',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/damenleeturks/128.jpg',    
    name: 'API Unit Test',
    deadline: '2203910',
    engagement: 30,
    avgiine:663,
    color: 'teal'
  },
  {
    username: 'Caspergrl',
    avatar: 'https://s3.amazonaws.com/uifaces/faces/twitter/caspergrl/128.jpg',    
    name: 'Project Deploy',
    deadline: '62329',
    engagement: 15,
    avgiine:53,
    color: 'grey'
  },

];

const getProject = (limit) => {
  return (limit) ? Projects.slice(0, limit) : Projects;
};


export {
  Projects,
  getProject
};