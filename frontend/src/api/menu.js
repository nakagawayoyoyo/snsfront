const Menu =  [
  { header: 'アプリ' },
  {
    title: 'アクティビティ',
    group: 'apps',
    icon: 'dashboard',
    name: 'Dashboard',
  },
  {
    title: 'アカウント管理',
    group: 'apps',
    icon: 'assignment_ind',
    name: 'Account',
  },
  {
    title: '履歴',
    group: 'apps',
    icon: 'history',
    name: 'History',
  },
  {
    title: 'ランキング',
    group: 'apps',
    icon: 'stars',
    name: 'Ranking',
  },
  {
    title: 'タブテスト',
    group: 'apps',
    icon: 'assignment_ind',
    name: 'Tabtest',
  },
  { header: '有料機能' },
  {
    title: 'マネジメント',
    group: 'apps',
    icon: 'group',
    name: 'Management',
  },
  {
    title: '自動いいねbot',
    group: 'apps',
    icon: 'event_seat',
    name: 'Marketingbot',
  },
  {
    title: '登録',
    group: 'apps',
    icon: '',
    name: 'Registration',
  },
];

// reorder menu
Menu.forEach((item) => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
});

export default Menu;
