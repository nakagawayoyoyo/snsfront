const posts =  [
  {
    title: '',
    desc: 'TISSOTさんが日本初のコンセプトショップを代官山にオープンなされたとの事で、レセプションパーティーにご招待頂きました😆🎉 ',
    featuredImage: '/static/discover_word/thumb/ds_1.jpg',
    date: '2日前',
    createdAt: new Date().toLocaleDateString(),
    hashtag:'#インスタ映え #インスタばえ ﻿#インフルエンサー #フリーモデル #モデル #フォローミー #follow #カメラ好き #写真撮っている人と繋がりたい﻿ #ファインダー越しの私の世界 #写真好きな人と繋がりたい #ファッション #服好きな人と繋がりたい #ファッション好きな人と繋がりたい #オシャレさんと繋がりたい #お洒落さんと繋がりたい #お洒落な人と繋がりたい #おしゃれさんと繋がりたい #ジム #腹筋 #シックスパック #筋肉'

  },
  {
    title: '',
    desc: 'TISSOTさんが日本初のコンセプトショップを代官山にオープンなされたとの事で、レセプションパーティーにご招待頂きました😆🎉 ',
    featuredImage: '/static/discover_word/thumb/ds_2.jpg',
    date: '3日前',
    createdAt: new Date().toLocaleDateString(),
    hashtag:'#ジョジョの奇妙な冒険 #荒木飛呂彦 先生 #リアルジョジョ #ジョジョ立ち風 #ジョジョ立ち #ジョジョ #ジョジョ好き #新国立美術館 #jojonokimyounabouken #jojosbizarreadventure #現代美術 #現代アート #モダンアート#インスタ映え #インスタばえ ﻿#インフルエンサー #フリーモデル #モデル #写真は心のシャッター #カメラ男子﻿ #ファインダー越しの私の世界 #写真好きな人と繋がりたい #ジム #トレーニング #筋トレ #腹筋 #シックスパック #筋肉'
  },
  {
    title: '',
    desc: 'TISSOTさんが日本初のコンセプトショップを代官山にオープンなされたとの事で、レセプションパーティーにご招待頂きました😆🎉 ',
    featuredImage: '/static/discover_word/thumb/ds_3.jpg',
    date: '5日前',
    createdAt: new Date().toLocaleDateString(),
    hashtag:'#huf#パーカー#ブーツ#網タイツ#ショーパン#ビジョビ'
  },
  {
    title: '',
    desc: 'TISSOTさんが日本初のコンセプトショップを代官山にオープンなされたとの事で、レセプションパーティーにご招待頂きました😆🎉 ',
    featuredImage: '/static/discover_word/thumb/ds_4.jpg',
    date: '7日前',
    createdAt: new Date().toLocaleDateString(),
    hashtag:'#ワンピース#ホワイトコーデ#寒い'
  },
];

const getPost = (limit) => {
  return (limit) ? posts.slice(0, limit) : posts;
};

export {
  getPost
};