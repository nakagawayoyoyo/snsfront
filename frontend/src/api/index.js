// implement your own methods in here, if your data is coming from A rest API
import * as Chart from './chart';
import * as Post from './post';
export default {
  // post
  getPOSTs: Post.getPost,
  // chart data
  getMonthVisit: Chart.monthVisitData,
  getSNSposts: Chart.postsSocialData,
  getFolowerData: Chart.getFolowerData,
};
