export default [
  {
    path: '*',
    meta: {
      public: true,
    },
    redirect: {
      path: '/404'
    }
  },
  {
    path: '/404',
    meta: {
      public: true,
    },
    name: 'NotFound',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/NotFound.vue`
    )
  },
  {
    path: '/403',
    meta: {
      public: true,
    },
    name: 'AccessDenied',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Deny.vue`
    )
  },
  {
    path: '/500',
    meta: {
      public: true,
    },
    name: 'ServerError',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Error.vue`
    )
  },
  {
    path: '/login',
    meta: {
      public: true,
    },
    name: 'Login',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Login.vue`
    )
  },
  {
    path: '/',
    meta: { breadcrumb: false },
    name: 'Root',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Dashboard.vue`
    )
  },
  {
    path: '/tabtest',
    meta: { breadcrumb: false },
    name: 'Tabtest',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Tabtest.vue`
    )
  },
  {
    path: '/layout/navigation-drawer',
    meta: { breadcrumb: false },
    name: 'components/navigation-drawers',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/layout/NavigationDrawers.vue`
    )
  },
  {
    path: '/dashboard',
    meta: { breadcrumb: false },
    name: 'Dashboard',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Dashboard.vue`
    )
  },
  {
    path: '/account',
    meta: { breadcrumb: false },
    name: 'Account',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Account.vue`
    )
  },
  {
    path: '/management',
    meta: { breadcrumb: false },
    name: 'Management',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Management.vue`
    )
  },
  {
    path: '/ranking',
    meta: { breadcrumb: false },
    name: 'Ranking',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Ranking.vue`
    )
  },
  {
    path: '/history',
    meta: { breadcrumb: false },
    name: 'History',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/History.vue`
    )
  },
  {
    path: '/botmarketing',
    meta: { breadcrumb: false },
    name: 'Marketingbot',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Marketingbot.vue`
    )
  },
  {
    path: '/registration',
    meta: { breadcrumb: false },
    name: 'Registration',
    component: () => import(
      /* webpackChunkName: "routes" */
      `@/views/Registration.vue`
    )
  },
];
